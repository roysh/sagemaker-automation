mkdir notebook

cd notebook

cdk init --language python --app notebook

cp ../app.py ../cdk.json  ../requirements.txt .

<update role in cdk.json>

source .env/bin/activate
pip install -r requirements.txt

cdk context

cdk list

cdk synth SagemakerStackEU

cdk deploy SagemakerStackEU

<uncomment bucket in app.py>

cdk diff SagemakerStackEU

cdk deploy SagemakerStackEU

cdk destroy SagemakerStackEU

