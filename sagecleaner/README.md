## SageCleaner: because sometimes, you need to wipe it all out!

*Disclaimer : this is provided as-is. 
This script works for me, but if it destroys your infrastructure, 
sets your car on fire or breaks your marriage, it'll be your fault, not mine.*

SageCleaner is a boto3-based Python script that lets you list, stop and delete
resources created by Amazon SageMaker <https://aws.amazon.com/sagemaker/>
  * notebook instances (stop or delete),
  * notebook instance lifecycle configurations,
  * repositories,
  * models (artefacts in Amazon S3 will not be deleted),
  * endpoint configurations,
  * endpoints.

You can run it on one, several, or all resource types.

You can run it in one, several, or all AWS regions.

You cannot list or delete *individual* resources: we have the AWS CLI for that.

Wipe away!

### Usage
```
python3 sagecleaner.py -h
usage: sagecleaner.py [-h] [-r REGIONS [REGIONS ...]] [-l LIST [LIST ...]]
                      [-s | -d] [-v]

List and delete Amazon SageMaker resources

optional arguments:
  -h, --help            show this help message and exit
  -r REGIONS [REGIONS ...], --regions REGIONS [REGIONS ...]
                        list of AWS regions: region name(s), or all
  -l LIST [LIST ...], --list LIST [LIST ...]
                        list resources: endpoints endpoint_configs
                        lifecycle_configs models notebook_instances
                        repositories, or all
  -s, --stop            stop notebook instances (ignored for other resources)
  -d, --delete          delete resources (no confirmation! you have been
                        warned)
  -v, --verbose         print API responses
```

### Examples
List all resources in all regions
```python3 sagecleaner.py --list all --regions all```

List endpoints in all regions
```python3 sagecleaner.py --list endpoints --region all```

List models and endpoint configs in us-east-1 and eu-west-1
```python3 sagecleaner.py --list models endpoint_configs --region us-east-1 eu-west-1```

Stop notebook instances in eu-west-1
```python3 sagecleaner.py --list notebook_instances --region eu-west-1 --stop```

Delete endpoints in eu-central-1 and eu-west-1
```python3 sagecleaner.py --list endpoints --region eu-central-1 eu-west-1 --delete```

Delete notebook instances in eu-west-1
```python3 sagecleaner.py --list notebook_instances --region eu-west-1 --delete```

Delete all resources in all regions (cue manic laugh)
```python3 sagecleaner.py --list all --regions all --delete```

